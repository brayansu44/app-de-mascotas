

//Modulos Internos

const express = require("express");
const mongoose = require("mongoose");

//Mofulos creados

const usuario = require("./routes/usuario");
const auth = require("./routes/auth");
const mascota = require("./routes/mascota");

// App

const app = express();
app.use(express.json());

//Rutas de mi api
app.use("/api/usuario", usuario);
app.use("/api/auth", auth);
app.use("/api/mascota", mascota);


//Puerto de ejecución
const port = process.env.PORT || 3001;
app.listen(port,() => console.log("... Escuchando el puerto: " + port));

mongoose
	.connect("mongodb://localhost/mascotasBD", {
		useNewUrlParser: true,
		useFindAndModify: false,
		useCreateIndex: true,
		useUnifiedTopology: true,
	})
	.then(() => console.log("Conexion a Mongo OK"))
	.catch((error) => console.log("Conexion a mongo OFF " + error));